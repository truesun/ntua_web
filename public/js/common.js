$(document).ready(function () {
    //=== 漢堡的開關 ===
    var switchBtn = false;
    var $hamburger = $("#hamBtn");
    var $hamburgerNav = $("#hamNavBtn");
    var $navSideArea = $("#navSideArea");
    $hamburger.on("click", function (e) {
        if (switchBtn == false) { //如果物件是關起來的就打開
            // NavAdd();
            $hamburger.hide();
            $navSideArea.addClass('open');
            // $hamburger.addClass('is-active');
            switchBtn = true;
        } else { //如果物件是打開的就關起來
            // NavRemove();
            $hamburger.show();
            // $hamburger.removeClass('is-active');
            switchBtn = false;
        }
    });
    $hamburgerNav.on("click", function (e) {
        $hamburger.show();
        $navSideArea.removeClass('open');
        switchBtn = false;
    });
    //=== hover 圖片輪播 ===
    $('.fade').on('mouseenter', function () {
        $(this).children('img').css({ 'opacity': 0 });
        $(this).find('.fade-img-box,.fade-img').show();
        $(this).find('.fade-img').addClass('on');
    });
    $('.fade').on('mouseleave', function () {
        $(this).children('img').removeAttr('style');
        $(this).find('.fade-img-box').removeAttr('style');
        $(this).find('.fade-img').removeClass('on');
    });

    //=== 圖片輪播(不限制張數)===
    // var timer;
    // $('.fade-area').each(function (ind, val) {
    //     var fadeImgL = $(val).find('.fade-box').length;
    //     var lastFade = fadeImgL - 1;
    //     $(val).find('.fade-box').each(function (ind, val) {
    //         $(val).addClass('cItem');
    //         if (ind == 0) {
    //             $(val).removeClass('cItem');
    //         } else if (ind == lastFade) {
    //             $(val).removeClass('cItem');
    //         }
    //     });
    // });
    // $('.fade-area').on('mouseenter', function () {
    //     $(this).children('img').css({ 'opacity': 0 });
    //     $(this).find('.fade-cont').show();
    //     var $itemBox = $(this);
    //     var iNow = 1;
    //     //- $('#test').find('.img').eq(0).show();
    //     $itemBox.find('.fade-box').eq(0).fadeIn(1000);
    //     //- $(this).find('.img').eq(iNow).addClass('fade1');
    //     //- $(this).find('.img').eq(iNow+1).addClass('fade2');
    //     timer = setInterval(function () {
    //         console.log('1', iNow)
    //         //- if(iNow!==1){
    //         //- //- 	$('#test').find('.img').eq(0).show();
    //         //- //- }
    //         //- //- else{
    //         //- 	$('#test').find('.img').eq(0).hide();
    //         //- }
    //         $itemBox.find('.fade-box').eq(iNow).fadeIn(1000);
    //         iNow++;
    //         var fadeImgL = $itemBox.find('.fade-box').length;
    //         var lastFade = fadeImgL - 1;
    //         if (iNow > fadeImgL) { //当到达最后一张图的时候，让iNow赋值为第一张图的索引值，轮播效果跳转到第一张图重新开始
    //             iNow = 1;
    //             $itemBox.find('.cItem').hide();
    //             $itemBox.find('.fade-box').eq(lastFade).fadeOut(1000);
    //             console.log(lastFade);
    //             $itemBox.find('.fade-box').eq(0).fadeIn(1000);
    //         }
    //     }, 2000);
    //     //- timer2=setInterval(function(){
    //     //- 	console.log('2',iNow)
    //     //- 	$('#test').find('.img').eq(iNow).fadeOut(500);
    //     //- 	iNow2=iNow2+2;
    //     //- },4000);
    //     //- $(this).find('.img').eq(iNow).addClass('on');
    //     //- changeDataAni();
    //     //- timer=setInterval(function(){ //打开定时器
    //     //- 	//- iNow++;  //让图片的索引值次序加1，这样就可以实现顺序轮播图片
    //     //- 	//- if(iNow>showNumber.length-1){ //当到达最后一张图的时候，让iNow赋值为第一张图的索引值，轮播效果跳转到第一张图重新开始
    //     //- 	//- 	iNow=0;
    //     //- 	//- }
    //     //- 	//- showNumber.eq(iNow).trigger("click"); //模拟触发数字按钮的click
    //     //- },2000); //2000为轮播的时间
    //     //- $(this).find('.fade-img-box,.fade-img').show();
    //     //- $(this).find('.fade-img').addClass('on');
    // });
    // $('.fade-area').on('mouseleave', function () {
    //     //== 暫停計時器 ==
    //     //- clearTimeout(timer);
    //     //== 清除計時器 ==
    //     clearInterval(timer);
    //     $(this).children('img').removeAttr('style');
    //     $(this).find('.fade-cont,.fade-box').hide();
    //     //- $(this).find('.fade-img-box').removeAttr('style');
    //     //- $(this).find('.fade-img').removeClass('on');
    // });

    //=== GOTOP ===
    $('#goTop').on('click', function () {
        $('html,body').animate({ scrollTop: 0 }, 'slow');
    });
});

$(window).scroll(function () {
    // var scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
    // var headerTop = $('#headerTop').offset().top;
    // var headerH = $('#header').outerHeight();
    // if (scrollTop > headerTop) {
    //     $('#header').css({ 'top': '0', 'position': 'fixed', 'width': '100%', 'z-index': 1 });
    //     $('#headerTop').css({ 'padding-bottom': headerH });
    // } else {
    //     $('#header,#headerTop').removeAttr('style');
    // }
});